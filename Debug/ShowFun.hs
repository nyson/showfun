{-# LANGUAGE RankNTypes, ScopedTypeVariables, KindSignatures, FlexibleInstances, CPP, TupleSections #-}
module Debug.ShowFun where

import Data.Typeable (Typeable, typeOf)
import Test.QuickCheck (generate, sample', Arbitrary, arbitrary)
import System.IO.Unsafe (unsafePerformIO)


instance {-# OVERLAPPING#-}
         ( Typeable a, Typeable b, Typeable c
         , Show a, Show b, Show c
         , Arbitrary a, Arbitrary b) => Show (a -> b -> c) where
  show f = header ++ concatMap (("\n" ++) . format) samples
           where
             samples :: [(a,b)]
             samples = unsafePerformIO (zip <$> sample' arbitrary <*> sample' arbitrary)
             format :: (a,b) -> String
             format = \(v1,v2) -> concat
               [ "f ", appendWS (longest $ map fst samples) (show v1)
               , " ", appendWS (longest $ map snd samples) (show v2)
               , " = ", show (f v1 v2)
               ]
             -- appendWS ch len str = str ++ replicate (len - length str) ' '
             header = "f :: " ++ show (typeOf f)

longest :: Show a => [a] -> Int
longest = maximum . map (length . show)

appendWS :: Int -> String -> String
appendWS len str = str ++ replicate (len - length str) ' '

instance {-# OVERLAPPING #-} ( Typeable a, Typeable b
                             , Show a, Show b
                             , Arbitrary a) => Show (a -> b) where
  show f = header ++ concatMap (("\n" ++) . format) samples
           where samples :: [a]
                 samples = unsafePerformIO (sample' arbitrary)
                 format = \val -> concat ["f ", appendWS (longest samples) (show val), " = ", show (f val)]
                 header = "f :: " ++ show (typeOf f)



-- Prelude System.IO.Unsafe Test.QuickCheck Data.Typeable Data.List> (++ " gunnar")
